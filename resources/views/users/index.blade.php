@extends('layout')

@section('content')
@if(!empty($users))
<table border="1" cellpadding="5">
	<tr>
		<td>Name</td>
		<td>Email</td>
		<td>Join at</td>
	</tr>
	@foreach($users as $user)
	<tr> 
		<td>{{ $user->name }}</td>
		<td>{{ $user->email }}</td>
		<td>{{ $user->created_at }}</td>
	</tr>
	@endforeach
</table>
@else
No Users 
@endif

@stop