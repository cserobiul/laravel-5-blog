@extends('layout')
@section('content')

@unless(empty($people))
There are some more people here.
@endunless

@foreach ($people as $person)
<li>{{$person}}</li>
@endforeach
<br>
@unless(empty($books))
There are many books here.
@endunless
@foreach($books as $book)
<li>{{$book}} </li>
@endforeach

@stop
