<?php

namespace App\Http\Controllers;
// use DB;
use App\Users;

use App\Http\Requests;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function index(){

    	// $users = \DB::table('users')->get();
    	$users = Users::all();
    	$user = [];

    	return view('users.index')->with([
    		'users' => $users
    		]);
    }
    public function show($id){
    	$user = Users::find($id);
    	return view('users.show')->with([
    		'user' => $user
    		]);
    	// return $user;
    }
}
