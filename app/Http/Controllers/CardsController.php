<?php

namespace App\Http\Controllers;

// use DB;
use App\Card;
use App\Http\Requests;
use Illuminate\Http\Request;

class CardsController extends Controller
{
	public function index(){
    	// $cards = DB::table('cards')->get();
		$cards = Card::all();
		return view('cards.index')->with([
			'cards' =>$cards
			]);
	}
    // public function show($id){
    // 	$card = Card::find($id);
    // 	return view('cards.show')->with([
    // 		'card'=>$card
    // 		]);
    // }
	public function show(Card $id){
		return view('cards.show')->with([
			'card'=>$id
			]);
	}
}
