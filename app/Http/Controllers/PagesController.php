<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PagesController extends Controller
{
	public function home(){
		$people = ['Arif','Khan','Joy','ami','tumi','she'];
		$books = ['c','c++','java','php','html','mysql'];

		return view('welcome')->with([
			'people'=> $people,
			'books' => $books
			]);
	}

	public function about(){
		$authors = ['Arif', 'Masum', 'Joy', 'Imon', 'Mama'];
		return view('pages.about')->with([
			'authors' => $authors
			]);
	}

	public function contact(){
		return view('pages.contact');
	}
}
