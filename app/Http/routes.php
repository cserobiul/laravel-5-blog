<?php

Route::get('/','PagesController@home');
Route::get('about','PagesController@about');
Route::get('contact','PagesController@contact');

Route::get('users','UsersController@index');
Route::get('users/{id}','UsersController@show');

Route::get('cards','CardsController@index');
Route::get('cards/{id}','CardsController@show');


Route::post('cards/{id}/notes','NotesController@store');